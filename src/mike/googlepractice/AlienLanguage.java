package mike.googlepractice;

import java.util.ArrayList;
import java.util.List;

public class AlienLanguage {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println(patterns("(xyz)abc(fgh)"));
	}	
	
	public static List<String> patterns(String pattern){
		
		//Convert pattern into stringbuilder
		List<String> combinations = new ArrayList<String>();
		
		if(pattern.contains("(")){
			//Find brackets
			int openingBracketIndex = pattern.indexOf('(');
			int closingBracketIndex = pattern.indexOf(')');
			char[] combinationsForBrackets = pattern.substring(openingBracketIndex + 1, closingBracketIndex).toCharArray();
			for (char c : combinationsForBrackets) {
				StringBuilder sb =  new StringBuilder(pattern);
				combinations.addAll(patterns(sb.replace(openingBracketIndex, closingBracketIndex + 1, c + "").toString()));
			}
			
		}else{
			combinations.add(pattern);
		}
		
		return combinations;
	}

}
