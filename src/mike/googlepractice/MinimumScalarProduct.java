package mike.googlepractice;

import java.util.Arrays;

public class MinimumScalarProduct {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] v1 = {1, 3, -5};
		Integer[] v2 = {-2, 4, 1};
		
		System.out.println(calculate(v1, v2));
	}

	
	public static int calculate(Integer[] vec1, Integer[] vec2){
		Arrays.sort(vec1);
		Arrays.sort(vec2);
		int sum = 0;
		for(int i = 0; i < vec1.length; i++){
			sum += vec1[i] * vec2[vec1.length - i - 1];
		}
		return sum;
	}
	
	public static <T> void reverse(T[] array){
		for(int i = 0; i < array.length / 2; i++){
			T temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
	}
}
