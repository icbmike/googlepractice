package mike.googlepractice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Mergesort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Integer[] arr = new Integer[] { 4, 7, 24, 23, 64, 84, 38, 23 };
		List<Integer> sortedList = mergesort(Arrays.asList(arr));
		for (int i : arr) {
			System.out.print(i + " ,");
		}
		System.out.println();
		for (int i : sortedList) {
			System.out.print(i + " ,");
		}
	}

	public static List<Integer> mergesort(List<Integer> l) {
		if (l.size() == 1) {
			return l;
		}
		int midpoint = l.size() / 2;
		List<Integer> left = l.subList(0, midpoint);
		List<Integer> right = l.subList(midpoint, l.size());
		return merge(mergesort(left), mergesort(right));

	}

	public static List<Integer> merge(List<Integer> left, List<Integer> right) {
		int rightIndex = 0;
		int leftIndex = 0;

		ArrayList<Integer> sortedList = new ArrayList<Integer>(left.size()
				+ right.size());

		while (leftIndex < left.size() && rightIndex < right.size()) {
			if (left.get(leftIndex) < right.get(rightIndex)) {
				sortedList.add(left.get(leftIndex++));
			} else {
				sortedList.add(right.get(rightIndex));
				rightIndex++;
			}
		}

		if (leftIndex < left.size()) {
			for (int i = leftIndex; i < left.size(); i++) {
				sortedList.add(left.get(i));
			}
		}

		if (rightIndex < right.size()) {
			for (int i = rightIndex; i < right.size(); i++) {
				sortedList.add(right.get(i));
			}
		}

		return sortedList;
	}

}
