#!/usr/bin/env python

def comparator(x, y):
	return x[1] - y[1]

def binary_search(list, value):

	left = 0
	right = len(list) - 1
	item = None

	if value > list[right][1] or value < list[left][1]:
		return item

	while not left > right:

		mid_item = list[(right - left) / 2 + left][1]
		if  mid_item < value:
			left = (right - left) / 2 + left + 1
		elif mid_item > value:
			right = (right - left) / 2 + left - 1
		else:
			#We found it
			item = list[(right - left) / 2 + left]
			break

	return item

def main():
	
	with open('a-small-practice.in') as f:

		num_test_cases = int(f.readline())
		for x in xrange(1, num_test_cases + 1):
			#GET all the infos
			c = int(f.readline())
			l = int(f.readline())
			prices = [(i, int(p)) for i, p in enumerate(f.readline().split())]
			
			prices.sort(comparator)
			for i, price in enumerate(prices):
				if price[1] < c:
					#find what we need to make this a match
					credit_left = c - price[1]
					match = binary_search(prices[i + 1:], credit_left)
					if not match is None:
						print "Case {0}: {1} {2}".format(x, price[0], match[0])
						break


if __name__ == '__main__':
	main()
