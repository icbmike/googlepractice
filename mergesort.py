from random import random


def main():
	l = [int(random() * 10) for x in xrange(0,10)]
	print l
	print mergesort(l)

def mergesort(list):

	if len(list) == 1:
		return list

	midpoint = len(list) / 2
	left = list[:midpoint]
	right = list[midpoint:]
	return merge(mergesort(left), (mergesort(right)))


def merge(left, right):
	leftIndex = 0
	rightIndex = 0
	sortedList = []

	while leftIndex < len(left) and rightIndex < len(right):

		if left[leftIndex] < right[rightIndex]:
			sortedList.append(left[leftIndex])
			leftIndex += 1
		else:
			sortedList.append(right[rightIndex])
			rightIndex += 1

	if leftIndex < len(left):
		#There is still stuff in left
		sortedList.extend(left[leftIndex:])
	elif rightIndex < len(right):
		sortedList.extend(right[rightIndex:])

	return sortedList


if __name__ == '__main__':
	main()