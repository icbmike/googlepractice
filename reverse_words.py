#!/usr/bin/env python

import sys


def reverse_words(input_str):
	return " ".join(input_str.split(' ')[::-1])

def main():
	if len(sys.argv) == 1:
		return

	s = " ".join(sys.argv[1:])
	print reverse_words(s)


if __name__ == '__main__':
	main()